DROP TABLE Fournir;
DROP TABLE Fournisseur;
DROP TABLE Reserver;
DROP TABLE Service;
DROP TABLE Site;
DROP TABLE Client;

--1. Création des tables :

CREATE TABLE Client(
Id_Client		char(7) PRIMARY KEY,
nom		varchar(30) NOT NULL,
adresse		varchar(150),
nationalite 	char(2),	
type		varchar(30) NOT NULL,
CONSTRAINT ck_type CHECK (type in ('Particulier','Entreprise'))
);

CREATE TABLE Site(
Id_Site		char(7) PRIMARY KEY,
adresse		varchar(150) NOT NULL,
pays 		char(2) NOT NULL
);

CREATE TABLE Service(
Id_Service	char(7) PRIMARY KEY,
type		varchar(30) NOT NULL,
stock		numeric NOT NULL,
designation	varchar(100) NOT NULL,
Id_Site		char(7) REFERENCES Site,
CONSTRAINT ck_type CHECK(type in ('Service','Bien')),
CONSTRAINT ck_stock CHECK(stock>=0)
);

CREATE TABLE Reserver(
Id_Client		char(7) REFERENCES Client,
Id_Service	char(7) REFERENCES Service,
Date_deb		date NOT NULL,
prix 		numeric NOT NULL,
duree 		numeric NOT NULL,
PRIMARY KEY(Id_Client,Id_Service,Date_deb),
CONSTRAINT ck_prix CHECK (prix>=0 AND prix=trunc(prix,2)),
CONSTRAINT ck_duree CHECK (duree=floor(duree))
);

CREATE TABLE Fournisseur(
Id_Fournisseur	char(7) PRIMARY KEY,
nom		varchar(30) NOT NULL,
adresse		varchar(150) NOT NULL,
nationalite 	char(2) NOT NULL
);

CREATE TABLE Fournir(
Id_Fournisseur	char(7) REFERENCES Fournisseur,
Id_Service	char(7) REFERENCES Service,
PRIMARY KEY (Id_Fournisseur,Id_Service)
);

--2. Remplissage :

--Insertions de Clients

INSERT INTO Client
VALUES('C000001','Jacob Lance','07 Rue des Lilas, Roma','IT','Particulier');
INSERT INTO Client
VALUES('C000002','Yann Champax','478 Rue des Krainjes, n°63, Clermont-Ferrand, 63000','FR','Particulier');
INSERT INTO Client
VALUES('C000003','Jade Branbandte','231 allée de l''encyclopédie, Morestel, 38510','FR','Particulier');
INSERT INTO Client
VALUES('C000004','Jhônathan Fragmite','145 Boulevard des  ut, Limoge, 87000','FR','Particulier');
INSERT INTO Client
VALUES('C000005','Tom Palah','03 Rue Ledra, Nicosia, 99010','CY','Entreprise');
INSERT INTO Client
VALUES('C000006','Beijing Corn','11 Shangdishi St, HaIdian District, Beijing, China, 100193','CN','Entreprise');
INSERT INTO Client
VALUES('C00007','Bob TheBuilder','23 rue des  travaux, Paris, France,75056','FR','Entreprise');


--Insertions de sites

INSERT INTO Site
VALUES('L000001','Piazza del Popolo, Roma','IT');
INSERT INTO Site
VALUES('L000002','Place de la république, Limoge','FR');
INSERT INTO Site
VALUES('L000003','Place de Jaude,Clermont-Ferrand','FR');
INSERT INTO Site
VALUES('L000004','Place Jacquard, Bourgoing Jalieu','FR');
INSERT INTO Site
VALUES('L000005','Place de la Grande-Paix-de-Montréal','CA');
INSERT INTO Site
VALUES('L000006','世纪公园 邮政编码, Pudong, Shanghai, China, 201204','CN');
INSERT INTO Site
VALUES('L000007','Vieux Port, Marseille','FR');
INSERT INTO Site
VALUES('L000008','CR225, 1261 Luxembourg','LU');
INSERT INTO Site
VALUES('L000009','1 Rue Dupleix, 75015 Paris','FR');
INSERT INTO Site
VALUES('L00000A','Godwin St, Port Moresby, Papouasie-Nouvelle-Guinée','PG');


--Insertions de Service

INSERT INTO Service
VALUES('S000001','Bien',5,'Fiat 500 (1957)','L000002');
INSERT INTO Service
VALUES('S000002','Bien',2,'Dacia Logan Utilitaire','L000003');
INSERT INTO Service
VALUES('S000003','Bien',3,'Costume','L000001');
INSERT INTO Service
VALUES('S000004','Bien',2,'Robe de mariée','L000005');
INSERT INTO Service
VALUES('S000005','Bien',5,'Panneau publicitaire 5*5m sur l''A1','L000008');
INSERT INTO Service
VALUES('S000006','Bien',5,'Panneau publicitaire 6*7m sur l''A8','L000007');
INSERT INTO Service
VALUES('S000007','Bien',0,'Appartement Clermont-Ferrand, 478 Rue des Krainjes n°63','L000003');
INSERT INTO Service
VALUES('S000008','Bien',1,'Appartement Paris, 08 Rue des Cycles n°207','L000009');
INSERT INTO Service
VALUES('S000009','Service',5,'Equipe Déménagement','L000006');
INSERT INTO Service
VALUES('S00000A','Service',2,'Equipe Installation','L000006');
INSERT INTO Service
VALUES('S00000B','Service',100,'Hébergement Web en Papouasie-Nouvelle-Guinée','L00000A');
INSERT INTO Service
VALUES('S00000C','Service',250,'Hébergement base de donnée à Clermont-Ferrand','L000003');
INSERT INTO Service
VALUES('S00000D','Bien',2,'Porsche 911 GT3 RS','L000004');

--Insertions de réservations

INSERT INTO Reserver
VALUES('C000001','S000003','01-07-2022','2500','2');
INSERT INTO Reserver
VALUES('C000002','S000007','07-07-2022','3750','125');
INSERT INTO Reserver
VALUES('C000003','S00000D','12-07-2022','5000','2');
INSERT INTO Reserver
VALUES('C000004','S000001','15-07-2022','840','7');
INSERT INTO Reserver
VALUES('C000005','S000006','01-08-2022','417','31');
INSERT INTO Reserver
VALUES('C000006','S000009','13-08-2022','180','1');
INSERT INTO Reserver
VALUES('C000002','S000002','03-09-2022','3000','180');
INSERT INTO Reserver
VALUES('C000005','S000005','16-09-2022','3000','360');
INSERT INTO Reserver
VALUES('C000001','S000003','29-09-2022','2500','2');
INSERT INTO Reserver
VALUES('C000002','S00000C','14-10-2022','7.20','61');
INSERT INTO Reserver
VALUES('C000003','S00000D','18-10-2022','15000','7');
INSERT INTO Reserver
VALUES('C000004','S000001','03-11-2022','360','3');
INSERT INTO Reserver
VALUES('C000002','S000007','09-11-2022','10950','365');
INSERT INTO Reserver
VALUES('C000006','S00000A','09-12-2022','360','2');

--Insertions de fournisseurs

INSERT INTO Fournisseur
VALUES('F000001','CentreAuto','26 Bd Charles de Gaulle, 63000 Clermont-Ferrand','FR');
INSERT INTO Fournisseur
VALUES('F000002','Taille''Heure','8352 Boul. Saint-Laurent, Montréal, QC H2P 2M3, Canada','CA');
INSERT INTO Fournisseur
VALUES('F000003','Daibu Communication','11, Shangdi 10th Street, HaIdian District, Beijing, 100085, China','CN');
INSERT INTO Fournisseur
VALUES('F000004','Daibu Manutention','11, Shangdi 10th Street, HaIdian District, Beijing, 100085, China','CN');
INSERT INTO Fournisseur
VALUES('F000005','Magn''A''part','Cayman Brac, Îles Caïmans','KY');
INSERT INTO Fournisseur
VALUES('F000006','Daibu Networks','11, Shangdi 10th Street, HaIdian District, Beijing, 100085, China','CN');

--Insertions dans Fournir

INSERT INTO Fournir
VALUES('F000001','S000001');
INSERT INTO Fournir
VALUES('F000001','S000002');
INSERT INTO Fournir
VALUES('F000001','S00000D');
INSERT INTO Fournir
VALUES('F000002','S000003');
INSERT INTO Fournir
VALUES('F000002','S000004');
INSERT INTO Fournir
VALUES('F000003','S000005');
INSERT INTO Fournir
VALUES('F000003','S000006');
INSERT INTO Fournir
VALUES('F000004','S000009');
INSERT INTO Fournir
VALUES('F000004','S00000A');
INSERT INTO Fournir
VALUES('F000005','S000007');
INSERT INTO Fournir
VALUES('F000005','S000008');
INSERT INTO Fournir
VALUES('F000006','S00000B');
INSERT INTO Fournir
VALUES('F000006','S00000C');

--3. Requêtes :

SELECT designation
FROM Service
WHERE Id_Service NOT IN (SELECT Id_Service FROM Reserver);

SELECT COUNT(Id_Service)
FROM Reserver R, Client C
WHERE C.Id_Client=R.Id_Client AND C.type='Entreprise' AND CURRENT_DATE>Date_deb AND CURRENT_DATE-7<Date_deb;

SELECT nom
FROM Client
WHERE Id_Client NOT IN (SELECT Id_Client FROM Reserver);

SELECT
(
    SELECT SUM(R.prix)
    FROM Reserver R, Client C
    WHERE R.Id_Client=C.Id_Client
    AND C.type='Entreprise'
    AND CURRENT_DATE>=R.Date_deb AND CURRENT_DATE-30<=R.Date_deb
),
(
    SELECT SUM(R.prix)
    FROM Reserver R, Client C
    WHERE R.Id_Client=C.Id_Client
    AND C.type='Particulier' 
    AND CURRENT_DATE>=R.Date_deb AND CURRENT_DATE-30<=R.Date_deb
);
